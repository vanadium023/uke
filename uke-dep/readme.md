#### 前端依赖备份

##### 版本不同不能直接使用, 需要进行适配

- [react-app-seed](https://github.com/SANGET/react-app-seed.git)
- [basic-helper](https://github.com/SANGET/basic-helper-js.git)
- [uke-cli](https://github.com/SANGET/uke-cli.git)
- [uke-request](https://github.com/SANGET/uke-request.git)
- [uke-admin-web-scaffold](https://github.com/SANGET/uke-admin-web-scaffold.git)
- [uke-admin-seed](https://github.com/SANGET/uke-admin-seed.git)

##### 对应关系

- [react-app-seed] -> react-app-seed
- [basic-helper] -> base-func
- [uke-cli] -> page-generator
- [uke-request] -> request
- [uke-admin-web-scaffold] -> admin-scaffold
- [uke-admin-seed] -> admin-dashboard
